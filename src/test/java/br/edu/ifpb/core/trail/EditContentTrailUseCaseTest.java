package br.edu.ifpb.core.trail;

import br.edu.ifpb.core.domain.Trail;
import br.edu.ifpb.core.usecases.trail.EditContentTrailUseCase;
import br.edu.ifpb.data.repository.interfaces.ITrailRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class EditContentTrailUseCaseTest {

    private ITrailRepository trailRepository;
    private EditContentTrailUseCase editContentTrailUseCase;

    @BeforeEach
    public void setUp() {
        trailRepository = mock(ITrailRepository.class);
        editContentTrailUseCase = new EditContentTrailUseCase(trailRepository);
    }

    @Test
    void testEditContentSuccessfully() {
        int trailId = 1;
        String newContent = "new content";
        Trail trail = new Trail();
        trail.setContent("old content");
        when(trailRepository.findTrailById(trailId)).thenReturn(Optional.of(trail));

        editContentTrailUseCase.execute(trailId, newContent);
        assertEquals(newContent, trail.getContent());
        verify(trailRepository, times(1)).update(any(Trail.class));
    }
}
