package br.edu.ifpb.core.trail;

import br.edu.ifpb.core.domain.Trail;
import br.edu.ifpb.core.usecases.trail.EditTitleTrailUseCase;
import br.edu.ifpb.data.repository.interfaces.ITrailRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class EditTitleTrailUseCaseTest {

    private ITrailRepository trailRepository;
    private EditTitleTrailUseCase editTitleTrailUseCase;

    @BeforeEach
    public void setUp() {
        trailRepository = mock(ITrailRepository.class);
        editTitleTrailUseCase = new EditTitleTrailUseCase(trailRepository);
    }

    @Test
    void testEditTitleSuccessfully() {
        int trailId = 1;
        String newTitle = "Novo Título";
        Trail trail = new Trail();
        when(trailRepository.findTrailById(trailId)).thenReturn(Optional.of(trail));

        editTitleTrailUseCase.execute(trailId, newTitle);

        assertEquals(newTitle, trail.getTitle());
        verify(trailRepository, times(1)).update(any(Trail.class));
    }
}
