package br.edu.ifpb.core.trail;

import br.edu.ifpb.core.domain.Question;
import br.edu.ifpb.core.domain.Trail;
import br.edu.ifpb.core.usecases.trail.CreateTrailUseCase;
import br.edu.ifpb.data.repository.interfaces.ITrailRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class CreateTrailUseCaseTest {

    private ITrailRepository trailRepository;
    private CreateTrailUseCase createTrailUseCase;

    @BeforeEach
    public void setUp() {
        trailRepository = mock(ITrailRepository.class);
        createTrailUseCase = new CreateTrailUseCase(trailRepository);
    }

    @Test
    void testCreateTrailSuccessfully() {
        String title = "Trilha 1";
        String content = "Conteúdo da trilha";
        ArrayList<Question> questions = new ArrayList<>();

        createTrailUseCase.execute(title, content, questions);

        verify(trailRepository, times(1)).save(any(Trail.class));
    }
}
