package br.edu.ifpb.core.user;

import br.edu.ifpb.core.domain.User;
import br.edu.ifpb.core.exceptions.IncorrectPasswordException;
import br.edu.ifpb.core.exceptions.UserNotFoundException;
import br.edu.ifpb.core.usecases.user.EditUserPasswordUseCase;
import br.edu.ifpb.data.repository.interfaces.IUserRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class EditUserPasswordUseCaseTest {

    private IUserRepository repository;
    private EditUserPasswordUseCase editUserPasswordUseCase;

    @BeforeEach
    public void setUp() {
        repository = mock(IUserRepository.class);
        editUserPasswordUseCase = new EditUserPasswordUseCase(repository);
    }

    @Test
    void testEditPasswordSuccessfully() {
        int userId = 1;
        String currentPassword = "01234567";
        String newPassword = "12345678";
        User user = new User("userName", currentPassword, false);

        when(repository.findUserById(userId)).thenReturn(Optional.of(user));

        editUserPasswordUseCase.execute(userId, currentPassword, newPassword);

        assertEquals(newPassword, user.getPassword());
        verify(repository, times(1)).update(user);
    }

    @Test
    void testUserNotFound() {
        int userId = 1;
        String currentPassword = "01234567";
        String newPassword = "12345678";

        when(repository.findUserById(userId)).thenReturn(Optional.empty());

        assertThrows(UserNotFoundException.class, () -> {
            editUserPasswordUseCase.execute(userId, currentPassword, newPassword);
        });
        verify(repository, never()).update(any(User.class));
    }

    @Test
    void testIncorrectPassword() {
        // Arrange
        int userId = 1;
        String correctPassword = "01234567";
        String incorrectPassword = "wrongPassword";
        String newPassword = "12345678";
        User user = new User("userName", correctPassword, false);

        when(repository.findUserById(userId)).thenReturn(Optional.of(user));

        assertThrows(IncorrectPasswordException.class, () -> {
            editUserPasswordUseCase.execute(userId, incorrectPassword, newPassword);
        });
        verify(repository, never()).update(any(User.class));
    }
}
