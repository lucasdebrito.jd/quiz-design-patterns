package br.edu.ifpb.core.user;

import br.edu.ifpb.core.domain.User;
import br.edu.ifpb.core.exceptions.UsernameAlreadyExistsException;
import br.edu.ifpb.core.usecases.user.CreateUserUseCase;
import br.edu.ifpb.data.repository.interfaces.IUserRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CreateUserUseCaseTest {

    private IUserRepository repository;
    private CreateUserUseCase createUserUseCase;

    @BeforeEach
    public void setUp() {
        repository = mock(IUserRepository.class);
        createUserUseCase = new CreateUserUseCase(repository);
    }

    @Test
    void testCreateUserSuccessfully() {
        // Arrange
        String username = "newUser";
        String password = "password";
        boolean isAdmin = false;

        when(repository.findUserByUsername(username)).thenReturn(Optional.empty());

        createUserUseCase.execute(username, password, isAdmin);

        verify(repository, times(1)).save(any(User.class));
    }

    @Test
    void testUsernameAlreadyExists() {
        String username = "existingUser";
        String password = "password";
        boolean isAdmin = false;

        User existingUser = new User(username, password, isAdmin);
        when(repository.findUserByUsername(username)).thenReturn(Optional.of(existingUser));

        assertThrows(UsernameAlreadyExistsException.class, () -> {
            createUserUseCase.execute(username, password, isAdmin);
        });
        verify(repository, never()).save(any(User.class));
    }
}
