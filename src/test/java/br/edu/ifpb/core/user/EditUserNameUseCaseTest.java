package br.edu.ifpb.core.user;

import br.edu.ifpb.core.domain.User;
import br.edu.ifpb.core.exceptions.IncorrectPasswordException;
import br.edu.ifpb.core.exceptions.UserNotFoundException;
import br.edu.ifpb.core.usecases.user.EditUserNameUseCase;
import br.edu.ifpb.data.repository.interfaces.IUserRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class EditUserNameUseCaseTest {

    private IUserRepository repository;
    private EditUserNameUseCase editUserNameUseCase;

    @BeforeEach
    public void setUp() {
        repository = mock(IUserRepository.class);
        editUserNameUseCase = new EditUserNameUseCase(repository);
    }

    @Test
    void testEditUserNameSuccessfully() {
        int userId = 1;
        String currentPassword = "password";
        String newName = "newName";
        User user = new User("oldName", currentPassword, false);

        when(repository.findUserById(userId)).thenReturn(Optional.of(user));

        editUserNameUseCase.execute(userId, currentPassword, newName);

        assertEquals(newName, user.getUserName());
        verify(repository, times(1)).update(user);
    }

    @Test
    void testUserNotFound() {
        int userId = 1;
        String currentPassword = "password";
        String newName = "newName";

        when(repository.findUserById(userId)).thenReturn(Optional.empty());

        assertThrows(UserNotFoundException.class, () -> {
            editUserNameUseCase.execute(userId, currentPassword, newName);
        });
        verify(repository, never()).update(any(User.class));
    }

    @Test
    void testIncorrectPassword() {
        int userId = 1;
        String correctPassword = "password";
        String incorrectPassword = "wrongPassword";
        String newName = "newName";
        User user = new User("oldName", correctPassword, false);

        when(repository.findUserById(userId)).thenReturn(Optional.of(user));

        assertThrows(IncorrectPasswordException.class, () -> {
            editUserNameUseCase.execute(userId, incorrectPassword, newName);
        });
        verify(repository, never()).update(any(User.class));
    }
}
