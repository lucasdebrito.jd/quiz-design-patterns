package br.edu.ifpb.core.question;

import br.edu.ifpb.core.domain.Alternative;
import br.edu.ifpb.core.domain.Question;
import br.edu.ifpb.core.domain.Trail;
import br.edu.ifpb.core.usecases.question.Alternatives;
import br.edu.ifpb.core.usecases.question.CreateQuestionUseCase;
import br.edu.ifpb.data.repository.interfaces.IQuestionRepository;
import br.edu.ifpb.data.repository.interfaces.ITrailRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class CreateQuestionUseCaseTest {

    private IQuestionRepository questionRepository;
    private ITrailRepository trailRepository;
    private CreateQuestionUseCase createQuestionUseCase;

    @BeforeEach
    public void setUp() {
        questionRepository = mock(IQuestionRepository.class);
        trailRepository = mock(ITrailRepository.class);
        createQuestionUseCase = new CreateQuestionUseCase(questionRepository, trailRepository);
    }

    @Test
    void testCreateQuestionSuccessfully() {
        String description = "Descrição da Questão";
        int trailId = 1;
        Trail trail = new Trail();
        when(trailRepository.findTrailById(trailId)).thenReturn(Optional.of(trail));

        Alternatives alternatives = new Alternatives();
        alternatives.add(new Alternative("Alternative 1", true));
        alternatives.add(new Alternative("Alternative 2", false));

        createQuestionUseCase.execute(description, trailId, alternatives);

        verify(questionRepository, times(1)).save(any(Question.class));
    }
}
