package br.edu.ifpb.core.question;

import br.edu.ifpb.core.domain.Alternative;
import br.edu.ifpb.core.exceptions.QuestionNotFoundException;
import br.edu.ifpb.core.usecases.question.AddAlternativeUseCase;
import br.edu.ifpb.data.repository.interfaces.IQuestionRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class AddAlternativeUseCaseTest {

    private IQuestionRepository questionRepository;
    private AddAlternativeUseCase addAlternativeUseCase;

    @BeforeEach
    public void setUp() {
        questionRepository = mock(IQuestionRepository.class);
        addAlternativeUseCase = new AddAlternativeUseCase(questionRepository);
    }

    @Test
    void testAddAlternativeSuccessfully() {
        int questionId = 1;
        String content = "Nova Alternativa";
        boolean isCorrect = true;

        when(questionRepository.verifyIfQuestionExists(questionId)).thenReturn(true);

        addAlternativeUseCase.execute(questionId, content, isCorrect);

        verify(questionRepository, times(1)).saveAlternative(eq(questionId), any(Alternative.class));
    }

    @Test
    void testAddAlternativeThrowsQuestionNotFoundException() {
        int questionId = 1;
        when(questionRepository.verifyIfQuestionExists(questionId)).thenReturn(false);

        assertThrows(QuestionNotFoundException.class, () -> {
            addAlternativeUseCase.execute(questionId, "Content", true);
        });
    }
}
