package br.edu.ifpb.core.question;

import br.edu.ifpb.core.domain.Question;
import br.edu.ifpb.core.exceptions.QuestionNotFoundException;
import br.edu.ifpb.core.usecases.question.EditDescriptionUseCase;
import br.edu.ifpb.data.repository.QuestionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class EditDescriptionUseCaseTest {

    private QuestionRepository questionRepository;
    private EditDescriptionUseCase editDescriptionUseCase;

    @BeforeEach
    public void setUp() {
        questionRepository = mock(QuestionRepository.class);
        editDescriptionUseCase = new EditDescriptionUseCase(questionRepository);
    }

    @Test
    void testEditDescriptionSuccessfully() {
        int questionId = 1;
        String newDescription = "Nova Descrição";
        Question question = new Question();
        when(questionRepository.getById(questionId)).thenReturn(Optional.of(question));

        editDescriptionUseCase.execute(questionId, newDescription);
        assertEquals(newDescription, question.getDescription());
        verify(questionRepository, times(1)).update(question);
    }

    @Test
    void testEditDescriptionThrowsQuestionNotFoundException() {
        int questionId = 1;
        when(questionRepository.getById(questionId)).thenReturn(Optional.empty());

        assertThrows(QuestionNotFoundException.class, () -> {
            editDescriptionUseCase.execute(questionId, "Nova Descrição");
        });
    }
}
