package br.edu.ifpb.core.question;

import br.edu.ifpb.core.domain.Alternative;
import br.edu.ifpb.core.exceptions.AlternativeNotFoundException;
import br.edu.ifpb.core.usecases.question.DeleteAlternativeUseCase;
import br.edu.ifpb.data.repository.QuestionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class DeleteAlternativeUseCaseTest {

    private QuestionRepository questionRepository;
    private DeleteAlternativeUseCase deleteAlternativeUseCase;

    @BeforeEach
    public void setUp() {
        questionRepository = mock(QuestionRepository.class);
        deleteAlternativeUseCase = new DeleteAlternativeUseCase(questionRepository);
    }

    @Test
    void testDeleteAlternativeSuccessfully() {
        int alternativeId = 1;
        Alternative alternative = new Alternative("Alternativa 1", false);
        when(questionRepository.getAlternativeById(alternativeId)).thenReturn(Optional.of(alternative));

        deleteAlternativeUseCase.execute(alternativeId);

        verify(questionRepository, times(1)).deleteAlternative(alternativeId);
    }

    @Test
    void testDeleteAlternativeThrowsAlternativeNotFoundException() {
        int alternativeId = 1;
        when(questionRepository.getAlternativeById(alternativeId)).thenReturn(Optional.empty());

        assertThrows(AlternativeNotFoundException.class, () -> {
            deleteAlternativeUseCase.execute(alternativeId);
        });
    }
}
