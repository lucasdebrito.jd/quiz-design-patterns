package br.edu.ifpb.core.question;

import br.edu.ifpb.core.exceptions.QuestionNotFoundException;
import br.edu.ifpb.core.usecases.question.DeleteQuestionUseCase;
import br.edu.ifpb.data.repository.QuestionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class DeleteQuestionUseCaseTest {

    private QuestionRepository questionRepository;
    private DeleteQuestionUseCase deleteQuestionUseCase;

    @BeforeEach
    public void setUp() {
        questionRepository = mock(QuestionRepository.class);
        deleteQuestionUseCase = new DeleteQuestionUseCase(questionRepository);
    }

    @Test
    void testDeleteQuestionSuccessfully() {
        int questionId = 1;
        when(questionRepository.verifyIfQuestionExists(questionId)).thenReturn(true);

        deleteQuestionUseCase.execute(questionId);

        verify(questionRepository, times(1)).delete(questionId);
    }

    @Test
    void testDeleteQuestionThrowsQuestionNotFoundException() {
        int questionId = 1;
        when(questionRepository.verifyIfQuestionExists(questionId)).thenReturn(false);

        assertThrows(QuestionNotFoundException.class, () -> {
            deleteQuestionUseCase.execute(questionId);
        });
    }
}
