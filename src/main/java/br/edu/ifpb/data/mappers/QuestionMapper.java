package br.edu.ifpb.data.mappers;

import br.edu.ifpb.core.domain.Alternative;
import br.edu.ifpb.core.domain.Question;
import br.edu.ifpb.core.domain.Trail;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class QuestionMapper {
    public static Question resultsetToQuestion(ResultSet resultSet, List<Alternative> alternatives) throws SQLException {
        int id = resultSet.getInt("id");
        String description = resultSet.getString("description");

        Trail trail = TrailMapper.resultsetToTrail(resultSet);
        return new Question(id, description, trail, alternatives);
    }

    public static Alternative resultsetToAlternative(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String content = resultSet.getString("content");
        boolean isCorrect = resultSet.getBoolean("is_correct");
        int questionId = resultSet.getInt("question_id");
        return new Alternative(id, content, isCorrect, questionId);
    }
}
