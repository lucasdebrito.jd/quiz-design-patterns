package br.edu.ifpb.data.mappers;

import br.edu.ifpb.core.domain.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper {
    public static User resultsetToUser(ResultSet resultSet) throws SQLException {
            String userName = resultSet.getString("user_name");
            String password = resultSet.getString("password");
            boolean isAdministrator = resultSet.getBoolean("is_administrator");
            return new User(userName, password, isAdministrator);
    }
}
