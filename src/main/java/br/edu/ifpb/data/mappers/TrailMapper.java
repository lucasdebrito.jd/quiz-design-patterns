package br.edu.ifpb.data.mappers;

import br.edu.ifpb.core.domain.Question;
import br.edu.ifpb.core.domain.Trail;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class TrailMapper {
    public static Trail resultsetToTrail(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String content = resultSet.getString("content");
        String title = resultSet.getString("title");
        return new Trail(id, title, content);
    }

    public static Trail resultsetToTrail(ResultSet resultSet, List<Question> questions) throws SQLException {
        int id = resultSet.getInt("id");
        String content = resultSet.getString("content");
        String title = resultSet.getString("title");
        return new Trail(id, title, content, questions);
    }
}
