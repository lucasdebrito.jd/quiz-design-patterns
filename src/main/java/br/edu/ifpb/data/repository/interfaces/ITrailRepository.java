package br.edu.ifpb.data.repository.interfaces;
import br.edu.ifpb.core.domain.Trail;

import java.util.Optional;

public interface ITrailRepository {
    public int save(Trail trail);
    public Optional<Trail> findTrailById(int id);
    public boolean update(Trail trail);
    public boolean delete(int trailId);
}
