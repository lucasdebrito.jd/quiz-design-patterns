package br.edu.ifpb.data.repository;

import br.edu.ifpb.data.db.TemboConnection;
import br.edu.ifpb.data.util.PreparedStatementSetter;
import br.edu.ifpb.data.util.ResultSetHandler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class Repository {
    protected <T> T executeQuery(String sql, PreparedStatementSetter setter, ResultSetHandler<T> handler) {
        Connection connection = TemboConnection.getInstance().getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            setter.setParameters(preparedStatement);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                return handler.handleResultSet(resultSet);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Erro ao executar consulta: " + e.getMessage(), e);
        }
    }

    protected void executeUpdate(String sql, PreparedStatementSetter setter) {
        Connection connection = TemboConnection.getInstance().getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            setter.setParameters(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("Erro ao executar atualização: " + e.getMessage(), e);
        }
    }
}
