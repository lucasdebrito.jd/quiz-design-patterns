package br.edu.ifpb.data.repository;

import br.edu.ifpb.core.domain.User;
import br.edu.ifpb.data.mappers.UserMapper;
import br.edu.ifpb.data.repository.interfaces.IUserRepository;
import br.edu.ifpb.data.util.QueryBuilder;

import java.util.Optional;

public class UserRepository extends Repository implements IUserRepository {

    @Override
    public void save(User user) {
        String sql = "INSERT INTO users (user_name, password, is_administrator) VALUES (?, ?, ?)";
        executeUpdate(sql, preparedStatement -> {
            preparedStatement.setString(1, user.getUserName());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setBoolean(3, user.isAdministrator());
        });
    }

    @Override
    public Optional<User> findUserById(int userId) {
        QueryBuilder queryBuilder = new QueryBuilder()
                .select("id")
                .select("user_name")
                .select("password")
                .select("is_administrator")
                .from("users")
                .where("id = ?");

        String sql = queryBuilder.build();

        return Optional.ofNullable(executeQuery(sql, preparedStatement -> preparedStatement.setInt(1, userId), resultSet -> {
            if (resultSet.next()) {
                return UserMapper.resultsetToUser(resultSet);
            }
            return null;
        }));
    }

    @Override
    public Optional<User> findUserByUsername(String username) {
        QueryBuilder queryBuilder = new QueryBuilder()
                .select("id")
                .select("user_name")
                .select("password")
                .select("is_administrator")
                .from("users")
                .where("user_name = ?");

        String sql = queryBuilder.build();

        return Optional.ofNullable(executeQuery(sql, preparedStatement -> preparedStatement.setString(1, username), resultSet -> {
            if (resultSet.next()) {
                return UserMapper.resultsetToUser(resultSet);
            }
            return null;
        }));
    }

    @Override
    public void update(User user) {
        String sql = "UPDATE users SET user_name = ?, password = ?, is_administrator = ? WHERE id = ?";
        executeUpdate(sql, preparedStatement -> {
            preparedStatement.setString(1, user.getUserName());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setBoolean(3, user.isAdministrator());
            preparedStatement.setInt(4, user.getId());
        });
    }
}
