package br.edu.ifpb.data.repository.interfaces;

import br.edu.ifpb.core.domain.Alternative;
import br.edu.ifpb.core.domain.Question;

import java.util.UUID;

public interface IQuestionRepository {
    public void save(Question question);
    public Question findQuestionById(UUID id);
    public void update(Question question);
    public void delete(UUID id);
    public boolean verifyIfQuestionExists(int questionId);
    public boolean saveAlternative(int questionId, Alternative newAlternative);
}
