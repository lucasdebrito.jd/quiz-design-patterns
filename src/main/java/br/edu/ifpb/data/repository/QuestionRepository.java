package br.edu.ifpb.data.repository;

import br.edu.ifpb.core.domain.Alternative;
import br.edu.ifpb.core.domain.Question;
import br.edu.ifpb.data.mappers.QuestionMapper;
import br.edu.ifpb.data.util.QueryBuilder;

import java.sql.ResultSet;
import java.util.Optional;
import java.util.ArrayList;

public class QuestionRepository extends Repository {

	private static final String ENTITY = "question";
	private static final String ALTERNATIVE_ENTITY = "alternative";

	public boolean delete(int questionId) {
		String sql = "DELETE FROM " + ENTITY + " WHERE id = ?";
		executeUpdate(sql, preparedStatement -> preparedStatement.setInt(1, questionId));

		return true;
	}

	public Optional<Question> getById(int questionId) {
		QueryBuilder queryBuilder = new QueryBuilder()
				.select("q.id")
				.select("q.description")
				.select("t.id AS trail_id")
				.select("t.content")
				.select(" t.title")
				.from("question q JOIN trail t ON q.trail_id = t.id")
				.where("q.id = ?");
		String questionSql = queryBuilder.build();

		QueryBuilder queryBuilder2 = new QueryBuilder()
				.select("id")
				.select("content")
				.select("is_correct")
				.select("question_id")
				.from("alternative")
				.where("question_id = ?");
		String alternativesSql = queryBuilder2.build();

		try {
			ArrayList<Alternative> alternatives = executeQuery(alternativesSql, preparedStatement -> preparedStatement.setInt(1, questionId), resultSet -> {
				ArrayList<Alternative> list = new ArrayList<>();
				while (resultSet.next()) {
					list.add(QuestionMapper.resultsetToAlternative(resultSet));
				}
				return list;
			});

			Optional<Question> questionOpt = Optional.ofNullable(executeQuery(questionSql, preparedStatement -> preparedStatement.setInt(1, questionId), resultSet -> {
                if (resultSet.next()) {
					return QuestionMapper.resultsetToQuestion(resultSet, alternatives);
                }
                return null;
            }));

			return questionOpt;
		} catch (Exception e) {
            throw new RuntimeException(e);
        }
	}

	public boolean verifyIfQuestionExists(int questionId) {
		String sql = "SELECT id FROM " + ENTITY + " WHERE id = ?";
		return executeQuery(sql, preparedStatement -> preparedStatement.setInt(1, questionId), ResultSet::next);
	}

	public int save(Question question) {
		String sql = "INSERT INTO " + ENTITY + " (description, trail_id) VALUES (?, ?) RETURNING id";
		return executeQuery(sql, preparedStatement -> {
			preparedStatement.setString(1, question.getDescription());
			preparedStatement.setInt(2, question.getTrail().getId());
		}, resultSet -> {
			if (resultSet.next()) {
				int questionId = resultSet.getInt("id");
				question.getAlternatives().forEach(alternative -> saveAlternative(questionId, alternative));
				return questionId;
			}
            return null;
        });
	}

	public void saveAlternative(int questionId, Alternative alternative) {
		String sql = "INSERT INTO " + ALTERNATIVE_ENTITY + " (content, is_correct, question_id) VALUES (?, ?, ?)";
		executeUpdate(sql, preparedStatement -> {
			preparedStatement.setString(1, alternative.getContent());
			preparedStatement.setBoolean(2, alternative.isCorrect());
			preparedStatement.setInt(3, questionId);
		});
	}

	public void deleteAlternative(int alternativeId) {
		String sql = "DELETE FROM " + ALTERNATIVE_ENTITY + " WHERE id = ?";
		executeUpdate(sql, preparedStatement -> preparedStatement.setInt(1, alternativeId));
	}

	public Optional<Alternative> getAlternativeById(int alternativeId) {
		String sql = "SELECT id, content, is_correct, question_id FROM " + ALTERNATIVE_ENTITY + " WHERE id = ?";
		return Optional.ofNullable(executeQuery(sql, preparedStatement -> preparedStatement.setInt(1, alternativeId), resultSet -> {
			if (resultSet.next()) {
                return QuestionMapper.resultsetToAlternative(resultSet);
			}
			return null;
		}));
	}

	public boolean update(Question question) {
		String sql = "UPDATE " + ENTITY + " SET description = ? WHERE id = ?";
		executeUpdate(sql, preparedStatement -> {
			preparedStatement.setString(1, question.getDescription());
			preparedStatement.setInt(2, question.getQuestionId());
		});

		return true;
	}

	public boolean updateAlternative(Alternative alternative) {
		String sql = "UPDATE " + ALTERNATIVE_ENTITY + " SET content = ?, is_correct = ? WHERE id = ?";
		executeUpdate(sql, preparedStatement -> {
			preparedStatement.setString(1, alternative.getContent());
			preparedStatement.setBoolean(2, alternative.isCorrect());
			preparedStatement.setInt(3, alternative.getAlternativeId());
		});

		return true;
	}
}
