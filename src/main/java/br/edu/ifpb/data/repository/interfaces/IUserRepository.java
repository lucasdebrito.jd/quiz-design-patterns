package br.edu.ifpb.data.repository.interfaces;
import br.edu.ifpb.core.domain.User;

import java.util.Optional;

public interface IUserRepository {
    public void save(User user);
    public Optional<User> findUserById(int id);
    public void update(User user);

    public Optional<User> findUserByUsername(String username);
}
