package br.edu.ifpb.data.repository;

import br.edu.ifpb.core.domain.Question;
import br.edu.ifpb.core.domain.Trail;
import br.edu.ifpb.data.mappers.TrailMapper;
import br.edu.ifpb.data.repository.interfaces.ITrailRepository;
import br.edu.ifpb.data.util.QueryBuilder;

import java.util.ArrayList;

import java.util.Optional;

public class TrailRepository extends Repository implements ITrailRepository {

    public Optional<Trail> findTrailById(int trailId) {
        QueryBuilder queryBuilder = new QueryBuilder()
                .select("id")
                .select("title")
                .select("content")
                .from("trail")
                .where("id = ?");

        String GET_TRAIL = queryBuilder.build();

        QueryBuilder queryBuilder2 = new QueryBuilder()
                .select("question.id")
                .from("question")
                .where("trail_id = ?");
        String GET_TRAIL_QUESTIONS = queryBuilder2.build();

        ArrayList<Question> trailQuestions = new ArrayList<>();

        executeQuery(GET_TRAIL_QUESTIONS, preparedStatement -> preparedStatement.setInt(1, trailId), resultSet -> {
            QuestionRepository questionRepository = new QuestionRepository();
            while (resultSet.next()) {
                int questionId = resultSet.getInt("id");
                questionRepository.getById(questionId).ifPresent(trailQuestions::add);
            }
            return null;
        });

        return Optional.ofNullable(executeQuery(GET_TRAIL, preparedStatement -> preparedStatement.setInt(1, trailId), resultSet -> {
            try {
                if (resultSet.next()) {
                    return TrailMapper.resultsetToTrail(resultSet, trailQuestions);
                }

                return null;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }));
    }

    public int save(Trail trail) {
        String sql = "INSERT INTO trail (content, title) values (?, ?) RETURNING id";

        return executeQuery(sql, preparedStatement -> {
            preparedStatement.setString(1, trail.getContent());
            preparedStatement.setString(2, trail.getTitle());
        }, resultSet -> {
            if (resultSet.next()) {
                int trailId = resultSet.getInt("id");

                QuestionRepository questionRepository = new QuestionRepository();
                trail.getQuestions().forEach(question -> {
                    question.getTrail().setId(trailId);
                    questionRepository.save(question);
                });

                return trailId;
            }
            throw new RuntimeException("Não foi possivel salvar a trilha");
        });
    }


    public boolean delete(int trailId) {
        String sql = "DELETE FROM trail WHERE id = ?";

        executeUpdate(sql, preparedStatement -> preparedStatement.setInt(1, trailId));

        return true;
    }

    public boolean update(Trail trail) {
        String sql = "UPDATE trail SET title = ?, content = ? WHERE id = ?";

        executeUpdate(sql, preparedStatement -> {
            preparedStatement.setString(1, trail.getTitle());
            preparedStatement.setString(2, trail.getContent());
            preparedStatement.setInt(3, trail.getId());
        });

        return true;
    }
}
