package br.edu.ifpb.data.db;

import io.github.cdimascio.dotenv.Dotenv;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public final class TemboConnection {
    private static final Logger logger = LoggerFactory.getLogger(TemboConnection.class);
    private static TemboConnection instance;
    private Connection connection;

    private TemboConnection() {
        connect();
    }

    private void connect() {
        try {
            Class.forName("org.postgresql.Driver");
            Dotenv dotenv = Dotenv.load();
            String DB_HOST = dotenv.get("DB_HOST");
            String DB_PORT = dotenv.get("DB_PORT");
            String DB_DATABASE = dotenv.get("DB_DATABASE");
            String DB_URL = "jdbc:postgresql://" + DB_HOST + ":" + DB_PORT + "/" + DB_DATABASE;
            String DB_USER = dotenv.get("DB_USER");
            String DB_PASSWORD = dotenv.get("DB_PASSWORD");

            this.connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            logger.info("Conexão ao banco de dados estabelecida com sucesso.");
        } catch (ClassNotFoundException e) {
            logger.error("Driver não encontrado: {}", e.getMessage());
        } catch (SQLException e) {
            logger.error("Erro ao conectar ao banco de dados: {}", e.getMessage());
        }
    }

    public void closeConnection() {
        if (connection != null) {
            try {
                connection.close();
                logger.info("Conexão ao banco de dados fechada com sucesso.");
            } catch (SQLException e) {
                logger.error("Erro ao fechar a conexão: {}", e.getMessage());
            }
        }
    }


//    public Statement createStatement() throws SQLException {
//        if (connection == null || connection.isClosed()) {
//            connect();
//        }
//        return connection.createStatement();
//    }

    public static TemboConnection getInstance() {
        if (instance == null) {
            instance = new TemboConnection();
        }
        return instance;
    }

    public Connection getConnection() {
        return connection;
    }
}
