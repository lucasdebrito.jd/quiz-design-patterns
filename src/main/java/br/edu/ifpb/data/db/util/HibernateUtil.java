package br.edu.ifpb.data.db.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import io.github.cdimascio.dotenv.Dotenv;

public class HibernateUtil {

    private static SessionFactory sessionFactory;

    static {
        try {
            Dotenv dotenv = Dotenv.load();

            // Configurar o Hibernate com as variáveis de ambiente
            Configuration cfg = new Configuration();
            cfg.setProperty("hibernate.connection.url", "jdbc:postgresql://" + dotenv.get("DB_HOST") + ":" + dotenv.get("DB_PORT") + "/" + dotenv.get("DB_DATABASE"));
            cfg.setProperty("hibernate.connection.username", dotenv.get("DB_USER"));
            cfg.setProperty("hibernate.connection.password", dotenv.get("DB_PASSWORD"));

            // Carregar a configuração do arquivo hibernate.cfg.xml (se necessário)
            cfg.configure(); // Isso irá carregar as outras propriedades do hibernate.cfg.xml, se existir

            // Criar a SessionFactory com a configuração carregada
            sessionFactory = cfg.buildSessionFactory();
        } catch (Throwable ex) {
            // Lança um erro caso a inicialização falhe
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void shutdown() {
        // Fechar a SessionFactory
        getSessionFactory().close();
    }
}
