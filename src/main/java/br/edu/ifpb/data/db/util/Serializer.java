package br.edu.ifpb.data.db.util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Serializer<T> {
    private static final Logger logger = LoggerFactory.getLogger(Serializer.class);
    private static final String ERROR_MESSAGE = "Erro: %s";
    private String path;

    public Serializer(String path) {
        this.path = path;
    }

    public void save(List<T> list) {
        File arq = new File(path);
        try {
            ObjectOutputStream objOutput = new ObjectOutputStream(new FileOutputStream(arq));
            objOutput.writeObject(list);
            objOutput.close();

        } catch (IOException erro) {
            logger.error(String.format(ERROR_MESSAGE, erro.getMessage()));
        }
    }

    @SuppressWarnings("unchecked")
    public ArrayList<T> read() {
        ArrayList list = new ArrayList();
        try {
            File arq = new File(path);
            if (arq.exists()) {
                ObjectInputStream objInput = new ObjectInputStream(new FileInputStream(arq));
                list = (ArrayList) objInput.readObject();
                objInput.close();
            }
        } catch (IOException erro1) {
            logger.error(String.format(ERROR_MESSAGE, erro1.getMessage()));
        } catch (ClassNotFoundException erro2) {
            logger.error(String.format(ERROR_MESSAGE, erro2.getMessage()));
        }

        return list;
    }
}
