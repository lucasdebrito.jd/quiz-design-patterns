package br.edu.ifpb.data.util;

import java.sql.ResultSet;

@FunctionalInterface
public interface ResultSetHandler<T> {
    T handleResultSet(ResultSet resultSet) throws Exception;
}
