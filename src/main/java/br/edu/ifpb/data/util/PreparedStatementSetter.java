package br.edu.ifpb.data.util;

import java.sql.PreparedStatement;
import java.sql.SQLException;

@FunctionalInterface
public interface PreparedStatementSetter {
    void setParameters(PreparedStatement preparedStatement) throws SQLException;
}
