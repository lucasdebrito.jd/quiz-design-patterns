package br.edu.ifpb.data.util;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class QueryBuilder {

    private String tableName;
    private List<String> selectColumns;
    private List<String> conditions;
    private String orderBy;
    private String limit;

    public QueryBuilder() {
        this.selectColumns = new ArrayList<>();
        this.conditions = new ArrayList<>();
    }

    public QueryBuilder select(String column) {
        this.selectColumns.add(column);
        return this;
    }

    public QueryBuilder from(String tableName) {
        this.tableName = tableName;
        return this;
    }

    public QueryBuilder where(String condition) {
        this.conditions.add(condition);
        return this;
    }

    public QueryBuilder orderBy(String orderBy) {
        this.orderBy = orderBy;
        return this;
    }

    public QueryBuilder limit(String limit) {
        this.limit = limit;
        return this;
    }

    public String build() {
        if (tableName == null || tableName.isEmpty()) {
            throw new IllegalStateException("Table name is mandatory.");
        }

        StringBuilder query = new StringBuilder("SELECT ");

        if (selectColumns.isEmpty()) {
            query.append("*");
        } else {
            StringJoiner joiner = new StringJoiner(", ");
            for (String column : selectColumns) {
                joiner.add(column);
            }
            query.append(joiner.toString());
        }

        query.append(" FROM ").append(tableName);

        if (!conditions.isEmpty()) {
            query.append(" WHERE ");
            StringJoiner joiner = new StringJoiner(" AND ");
            for (String condition : conditions) {
                joiner.add(condition);
            }
            query.append(joiner.toString());
        }

        if (orderBy != null) {
            query.append(" ORDER BY ").append(orderBy);
        }

        if (limit != null) {
            query.append(" LIMIT ").append(limit);
        }

        return query.toString();
    }
}
