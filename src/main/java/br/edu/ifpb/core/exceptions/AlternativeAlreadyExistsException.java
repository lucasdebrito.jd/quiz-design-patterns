package br.edu.ifpb.core.exceptions;

public class AlternativeAlreadyExistsException extends RuntimeException {
    public AlternativeAlreadyExistsException() {
        super("Alternativa ja existe!");
    }
}
