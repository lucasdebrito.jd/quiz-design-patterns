package br.edu.ifpb.core.exceptions;

public class AuthException extends Exception {
    public AuthException() {
        super("Autenticação falhou!");
    }
}
