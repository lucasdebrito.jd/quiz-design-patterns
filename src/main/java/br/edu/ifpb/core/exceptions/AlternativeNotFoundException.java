package br.edu.ifpb.core.exceptions;

public class AlternativeNotFoundException extends RuntimeException {
    public AlternativeNotFoundException(int alternativeId) {
        super("Alternativa com o id " + alternativeId + " não encontrada!");
    }
}
