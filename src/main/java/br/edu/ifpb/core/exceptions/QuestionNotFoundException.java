package br.edu.ifpb.core.exceptions;

public class QuestionNotFoundException extends RuntimeException {
    public QuestionNotFoundException() {
        super("Questão com id não encontrada!");
    }
}
