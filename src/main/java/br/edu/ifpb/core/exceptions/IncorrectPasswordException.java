package br.edu.ifpb.core.exceptions;

public class IncorrectPasswordException extends RuntimeException {
    public IncorrectPasswordException() {
        super("Senha incorreta!");
    }
}
