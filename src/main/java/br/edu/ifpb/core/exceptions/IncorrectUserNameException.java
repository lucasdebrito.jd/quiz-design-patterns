package br.edu.ifpb.core.exceptions;

public class IncorrectUserNameException extends RuntimeException {
    public IncorrectUserNameException() {
        super("Nome de usuário incorreto!");
    }
}
