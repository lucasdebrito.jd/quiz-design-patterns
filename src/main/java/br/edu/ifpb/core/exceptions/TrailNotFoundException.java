package br.edu.ifpb.core.exceptions;

public class TrailNotFoundException extends RuntimeException {
    public TrailNotFoundException() {
        super("Trilha não encontrada!");
    }
}
