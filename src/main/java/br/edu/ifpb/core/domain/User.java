package br.edu.ifpb.core.domain;

import java.io.Serializable;

public class User implements Serializable {
    private int id;
    private String userName;
    private String password;
    private boolean isAdministrator;

    public User(String userName, String password, boolean isAdministrator) {
        this.userName = userName;
        this.password = password;
        this.isAdministrator = isAdministrator;
    }

    public User() {

    }

    public boolean verifyPassword(String givenPassword) {
        return this.getPassword().equals(givenPassword);
    }


    public boolean isAdministrator() {
        return isAdministrator;
    }

    public void setAdministrator(boolean administrator) {
        isAdministrator = administrator;
    }

    public int getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
