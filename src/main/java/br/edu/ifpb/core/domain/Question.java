package br.edu.ifpb.core.domain;
import br.edu.ifpb.core.interfaces.IQuestion;

import java.io.Serializable;
import java.util.List;

public class Question implements IQuestion, Serializable {
    private int questionId;
    private String description;
    private Trail trail;

    private List<Alternative> alternatives;

    public Question(String description, Trail trail, List<Alternative> alternatives) {
        this.description = description;
        this.trail = trail;
        this.alternatives = alternatives;
    }

    public Question() {

    }

    public Question(int id, String description, Trail trail, List<Alternative> alternatives) {
        this.questionId = id;
        this.description = description;
        this.trail = trail;
        this.alternatives = alternatives;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Trail getTrail() {
        return trail;
    }

    public void setTrail(Trail trail) {
        this.trail = trail;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public List<Alternative> getAlternatives() {
        return alternatives;
    }

    public void setAlternatives(List<Alternative> alternatives) {
        this.alternatives = alternatives;
    }

    @Override
    public String toString() {
        return "Question{" +
                "questionId=" + questionId +
                ", description='" + description + '\'' +
                ", trail=" + trail +
                ", alternatives=" + alternatives +
                '}';
    }
}
