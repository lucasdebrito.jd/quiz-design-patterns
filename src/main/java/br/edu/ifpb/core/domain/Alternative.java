package br.edu.ifpb.core.domain;

import java.io.Serializable;

public class Alternative implements Serializable {
    private int alternativeId;
    private String content;
    private boolean isCorrect;
    private int questionId;

    public Alternative(int id, String content, boolean isCorrect, int questionId) {
        this.alternativeId = id;
        this.content = content;
        this.isCorrect = isCorrect;
        this.questionId = questionId;
    }

    public Alternative(String content, boolean isCorrect) {
        this.content = content;
        this.isCorrect = isCorrect;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }
    public int getAlternativeId() {
        return alternativeId;
    }

    @Override
    public String toString() {
        return "Alternative{" +
                "alternativeId=" + alternativeId +
                ", content='" + content + '\'' +
                ", isCorrect=" + isCorrect +
                ", questionId=" + questionId +
                '}';
    }
}
