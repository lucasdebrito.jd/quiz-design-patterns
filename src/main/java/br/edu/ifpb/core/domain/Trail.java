package br.edu.ifpb.core.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Trail implements Serializable {
    private int id;
    private String content;
    private String title;
    private List<Question> questions;

    public Trail(int id, String title, String content) {
        this.id = id;
        this.content = content;
        this.title = title;
        this.questions = new ArrayList<>();
    }

    public Trail() {
        
    }

    public Trail(int id, String title, String content, List<Question> questions) {
        this.id = id;
        this.content = content;
        this.title = title;
        this.questions = questions;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    @Override
    public String toString() {
        return "Trail{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", title='" + title + '\'' +
                ", questions=" + questions +
                '}';
    }
}
