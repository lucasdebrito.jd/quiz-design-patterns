package br.edu.ifpb.core.interfaces;

import java.util.List;

import br.edu.ifpb.core.domain.Alternative;
import br.edu.ifpb.core.domain.Trail;

public interface IQuestion {
    public void setDescription(String description);
    public String getDescription();
    public Trail getTrail();
    public void setTrail(Trail trail);
    public int getQuestionId();
    public void setQuestionId(int questionId);
    public List<Alternative> getAlternatives();
    public void setAlternatives(List<Alternative> alternatives);
}
