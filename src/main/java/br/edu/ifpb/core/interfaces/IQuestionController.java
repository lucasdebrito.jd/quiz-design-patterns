package br.edu.ifpb.core.interfaces;

import br.edu.ifpb.core.domain.Alternative;
import br.edu.ifpb.core.domain.Question;

import java.util.List;
import java.util.UUID;

public interface IQuestionController {
    public boolean isCorrectAlternative(UUID id);
    public void createQuestion(String description, List<Alternative> alternatives);
    public void deleteQuestion(UUID id);
    public void editDescription(UUID id, String newDescription);
    public void addAlternative(String content, boolean isCorrect);
    public void deleteAlternative(UUID id);
    public Question getQuestion(UUID id);
}
