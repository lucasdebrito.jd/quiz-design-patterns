package br.edu.ifpb.core.usecases.trail;

import br.edu.ifpb.core.domain.Trail;
import br.edu.ifpb.data.repository.interfaces.ITrailRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
// S
public class EditContentTrailUseCase {
    private static final Logger logger = LoggerFactory.getLogger(EditContentTrailUseCase.class);
    private ITrailRepository trailRepository;

    // D
    public EditContentTrailUseCase(ITrailRepository repository) {

        this.trailRepository = repository;
    }

    public void execute(int id, String newContent) {
        Trail trail = trailRepository.findTrailById(id).get();

        trail.setContent(newContent);
        trailRepository.update(trail);

        logger.info("Conteúdo da trilha editado com sucesso!");
    }
}
