package br.edu.ifpb.core.usecases.user;
import br.edu.ifpb.core.domain.User;
import br.edu.ifpb.core.exceptions.UsernameAlreadyExistsException;
import br.edu.ifpb.core.usecases.user.util.UserValidations;
import br.edu.ifpb.data.repository.interfaces.IUserRepository;

import java.util.Optional;

// S
public class CreateUserUseCase {
    private IUserRepository repository;

    // D
    public CreateUserUseCase(IUserRepository repository) {
        this.repository = repository;
    }

    public void execute(String userName, String password, boolean isAdministrator) {
        UserValidations.validateName(userName);
        UserValidations.validatePassword(password);
        Optional<User> existentUser = repository.findUserByUsername(userName);

        User user = new User(userName, password, isAdministrator);

        if (existentUser.isPresent()) {
            throw new UsernameAlreadyExistsException(userName);
        }

        repository.save(user);
    }
}
