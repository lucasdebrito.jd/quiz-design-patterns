package br.edu.ifpb.core.usecases.question;
import br.edu.ifpb.core.domain.Alternative;
import br.edu.ifpb.core.exceptions.QuestionNotFoundException;
import br.edu.ifpb.data.repository.interfaces.IQuestionRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AddAlternativeUseCase {
    private static final Logger logger = LoggerFactory.getLogger(AddAlternativeUseCase.class);
    private final IQuestionRepository questionRepository;

    // D
    public AddAlternativeUseCase(IQuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    // S
    public void execute(int questionId, String content, boolean isCorrect) {
        boolean questionExists = questionRepository.verifyIfQuestionExists(questionId);

        if(!questionExists) {
            throw new QuestionNotFoundException();
        }

        Alternative newAlternative = new Alternative(content, isCorrect);
        questionRepository.saveAlternative(questionId, newAlternative);

        logger.info("Alternativa adicionada com sucesso à questão!");
    }
}