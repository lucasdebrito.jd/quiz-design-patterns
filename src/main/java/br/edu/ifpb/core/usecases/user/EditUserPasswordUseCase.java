package br.edu.ifpb.core.usecases.user;

import br.edu.ifpb.core.domain.User;
import br.edu.ifpb.core.exceptions.IncorrectPasswordException;
import br.edu.ifpb.core.exceptions.UserNotFoundException;
import br.edu.ifpb.core.usecases.user.util.UserValidations;
import br.edu.ifpb.data.repository.interfaces.IUserRepository;

import java.util.Optional;

// S
public class EditUserPasswordUseCase {
    private IUserRepository repository;

    // D
    public EditUserPasswordUseCase(IUserRepository repository) {
        this.repository = repository;
    }

    public void execute(int id, String currentPassword, String newPassword) {
        UserValidations.validatePassword(newPassword);
        Optional<User> user = repository.findUserById(id);

        if (user.isEmpty()) {
            throw new UserNotFoundException();
        }

        if (!user.get().getPassword().equals(currentPassword)) {
            throw new IncorrectPasswordException();
        }

        user.get().setPassword(newPassword);
        repository.update(user.get());
    }
}
