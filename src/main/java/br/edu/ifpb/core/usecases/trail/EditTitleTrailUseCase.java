package br.edu.ifpb.core.usecases.trail;
import br.edu.ifpb.core.domain.Trail;
import br.edu.ifpb.data.repository.interfaces.ITrailRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
// S
public class EditTitleTrailUseCase {
    private static final Logger logger = LoggerFactory.getLogger(EditTitleTrailUseCase.class);
    private final ITrailRepository trailRepository;

    // D
    public EditTitleTrailUseCase(ITrailRepository repository) {
        this.trailRepository = repository;
    }

    public void execute(int id, String newTitle) {
        Trail trail = trailRepository.findTrailById(id).get();

        trail.setTitle(newTitle);
        trailRepository.update(trail);

        logger.info("Título da trilha editado com sucesso!");
    }
}
