package br.edu.ifpb.core.usecases.question;
import br.edu.ifpb.core.exceptions.QuestionNotFoundException;
import br.edu.ifpb.data.repository.QuestionRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class DeleteQuestionUseCase {
    private static final Logger logger = LoggerFactory.getLogger(DeleteQuestionUseCase.class);
    private final QuestionRepository questionRepository;

    // D
    public DeleteQuestionUseCase(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    // S
    public void execute(int questionId) {
        boolean questionExists = questionRepository.verifyIfQuestionExists(questionId);

        if (!questionExists) {
            throw new QuestionNotFoundException();
        }

        questionRepository.delete(questionId);
        logger.info("Questão removida com sucesso!");
    }
}
