package br.edu.ifpb.core.usecases.auth;

import br.edu.ifpb.core.domain.User;
import br.edu.ifpb.core.exceptions.AuthException;
import br.edu.ifpb.data.repository.interfaces.IUserRepository;

import java.util.Optional;

public class Auth implements AuthHandler {
    private AuthHandler next;
    private IUserRepository userRepository;

    public Auth(IUserRepository userRepository){
        this.userRepository = userRepository;
    }

    @Override
    public void execute(AuthRequest authRequest) throws AuthException {
        String givenName = authRequest.getUserName();
        String givenPassword = authRequest.getPassword();
        Optional<User> existingUser = userRepository.findUserByUsername(givenName);
        if(existingUser.isEmpty()){
            return;
        }

        User user = existingUser.get();
        boolean passwordMatches = user.verifyPassword(givenPassword);

        if(passwordMatches){
            authRequest.setAuthenticated(true);
            return;
        }

        throw new AuthException();
    }

    @Override
    public AuthHandler getNext() {
        return this.next;
    }

    @Override
    public void setNext(AuthHandler authHandler) {
        this.next = authHandler;
    }
}