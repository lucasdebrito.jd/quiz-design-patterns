package br.edu.ifpb.core.usecases.trail;
import br.edu.ifpb.core.domain.Question;
import br.edu.ifpb.core.domain.Trail;
import br.edu.ifpb.data.repository.interfaces.ITrailRepository;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// S
public class CreateTrailUseCase {
    private static final Logger logger = LoggerFactory.getLogger(CreateTrailUseCase.class);
    private final ITrailRepository trailRepository;

    // D
    public CreateTrailUseCase(ITrailRepository repository) {
        this.trailRepository = repository;
    }

    public void execute(String title, String content, List<Question> questions) {

        Trail trail = new Trail();
        trail.setTitle(title);
        trail.setContent(content);
        trail.setQuestions(questions);

        trailRepository.save(trail);

        logger.info("Trilha criada com sucesso!");
    }
}
