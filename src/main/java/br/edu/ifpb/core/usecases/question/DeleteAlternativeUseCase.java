package br.edu.ifpb.core.usecases.question;

import br.edu.ifpb.core.domain.Alternative;
import br.edu.ifpb.core.exceptions.AlternativeNotFoundException;
import br.edu.ifpb.data.repository.QuestionRepository;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteAlternativeUseCase {
    private static final Logger logger = LoggerFactory.getLogger(DeleteAlternativeUseCase.class);
    private final QuestionRepository questionRepository;

    // D
    public DeleteAlternativeUseCase(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    // S
    public void execute(int alternativeId) {
        Optional<Alternative> alternative = questionRepository.getAlternativeById(alternativeId);
        if (alternative.isEmpty()) {
            throw new AlternativeNotFoundException(alternativeId);
        }
        questionRepository.deleteAlternative(alternativeId);

       logger.info("Alternativa removida com sucesso da questão!");
    }
}
