package br.edu.ifpb.core.usecases.auth;

import br.edu.ifpb.core.exceptions.AuthException;

public interface AuthHandler {
    public void execute(AuthRequest authRequest) throws AuthException;
    public AuthHandler getNext();
    public void setNext(AuthHandler authHandler);
}
