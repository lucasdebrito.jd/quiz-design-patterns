package br.edu.ifpb.core.usecases.question;

import br.edu.ifpb.core.domain.Alternative;
import br.edu.ifpb.core.domain.Question;
import br.edu.ifpb.core.domain.Trail;
import br.edu.ifpb.data.repository.interfaces.IQuestionRepository;
import br.edu.ifpb.data.repository.interfaces.ITrailRepository;

import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateQuestionUseCase {
    private static final Logger logger = LoggerFactory.getLogger(CreateQuestionUseCase.class);
    private final IQuestionRepository questionRepository;
    private final ITrailRepository trailRepository;

    // D
    public CreateQuestionUseCase(IQuestionRepository questionRepository, ITrailRepository trailRepository) {
        this.questionRepository = questionRepository;
        this.trailRepository = trailRepository;
    }

    // S
    public void execute(String description, int trailId, Alternatives alternatives) {
        ArrayList<Alternative> arrayAlternatives = new ArrayList<>();

        arrayAlternatives.addAll(alternatives.getAlternatives());

        Trail trail = trailRepository.findTrailById(trailId).get();
        Question question = new Question(description, trail, arrayAlternatives);

        questionRepository.save(question);

        logger.info("Questão criada com sucesso!");
    }
}
