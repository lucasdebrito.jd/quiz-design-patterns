package br.edu.ifpb.core.usecases.user;

import br.edu.ifpb.core.domain.User;
import br.edu.ifpb.core.exceptions.UserNotFoundException;
import br.edu.ifpb.data.repository.interfaces.IUserRepository;
import br.edu.ifpb.core.exceptions.IncorrectUserNameException;

import java.util.Optional;

// S
public class LoginUseCase {
    // D
    public boolean execute(IUserRepository repository, String username, String password) {
        Optional<User> user = repository.findUserByUsername(username);

        if (user.isEmpty()) {
            throw new IncorrectUserNameException();
        }

        if (!user.get().getPassword().equals(password)) {
            throw new UserNotFoundException(); // trocar exceção
        }

        return true;
    }
}