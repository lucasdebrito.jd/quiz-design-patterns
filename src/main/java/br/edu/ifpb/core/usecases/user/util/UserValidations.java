package br.edu.ifpb.core.usecases.user.util;

import br.edu.ifpb.core.exceptions.InvalidDataException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserValidations {
    public static void validateName(String name) {
        Pattern pattern = Pattern.compile("[A-Za-zÀ-ÿ]{3,25}$");
        Matcher matcher = pattern.matcher(name);

        if (!matcher.matches()) {
            throw new InvalidDataException("Este username é inválido!");
        }
    }

    public static void validatePassword(String password) {
        Pattern pattern = Pattern.compile("^.{8}$");
        Matcher matcher = pattern.matcher(password);

        if (!matcher.matches()) {
            throw new InvalidDataException("Senha inválida! A senha deve possuir 8 caracteres");
        }
    }
}