package br.edu.ifpb.core.usecases.auth;

import br.edu.ifpb.core.exceptions.AuthException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CredentialsValidator implements AuthHandler {
    private AuthHandler next;

    @Override
    public void execute(AuthRequest authRequest) throws AuthException {
        String givenName = authRequest.getUserName().trim();
        String givenPassword = authRequest.getPassword().trim();
        Pattern stringPattern = Pattern.compile("[A-Za-zÀ-ÿ]{3,25}$");
        Matcher matcher = stringPattern.matcher(givenName);

        if (matcher.matches() && givenPassword.length() >= 8) {
            next.execute(authRequest);
            return;
        }

        throw new AuthException();
    }

    @Override
    public AuthHandler getNext() {
        return this.next;
    }

    @Override
    public void setNext(AuthHandler authHandler) {
        this.next = authHandler;
    }
}
