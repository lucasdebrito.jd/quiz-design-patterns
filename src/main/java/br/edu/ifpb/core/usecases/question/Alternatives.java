package br.edu.ifpb.core.usecases.question;

import br.edu.ifpb.core.domain.Alternative;

import java.util.HashSet;
import java.util.Set;

public class Alternatives {
    private Set<Alternative> alternativesList = new HashSet<>();

    public void add(Alternative alternative) {
        alternativesList.add(alternative);
    }

    public Set<Alternative> getAlternatives() {
        return alternativesList;
    }
}
