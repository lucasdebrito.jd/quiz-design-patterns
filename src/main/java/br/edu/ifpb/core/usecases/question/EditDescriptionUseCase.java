package br.edu.ifpb.core.usecases.question;

import br.edu.ifpb.core.domain.Question;
import br.edu.ifpb.core.exceptions.QuestionNotFoundException;
import br.edu.ifpb.data.repository.QuestionRepository;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EditDescriptionUseCase {
    private static final Logger logger = LoggerFactory.getLogger(EditDescriptionUseCase.class);
    private final QuestionRepository questionRepository;

    // D
    public EditDescriptionUseCase(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    // S
    public void execute(int questionId, String newDescription) {
        Optional<Question> question = questionRepository.getById(questionId);

        if (question.isEmpty()) {
            throw new QuestionNotFoundException();
        }

        Question existingQuestion = question.get();
        existingQuestion.setDescription(newDescription);

        questionRepository.update(existingQuestion);
        logger.info("Questão editada com sucesso!");
    }
}
